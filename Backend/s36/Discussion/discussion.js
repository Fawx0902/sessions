db.fruits.insertMany([
      {
        name : "Apple",
        color : "Red",
        stock : 20,
        price: 40,
        supplier_id : 1,
        onSale : true,
        origin: [ "Philippines", "US" ]
      },

      {
        name : "Banana",
        color : "Yellow",
        stock : 15,
        price: 20,
        supplier_id : 2,
        onSale : true,
        origin: [ "Philippines", "Ecuador" ]
      },

      {
        name : "Kiwi",
        color : "Green",
        stock : 25,
        price: 50,
        supplier_id : 1,
        onSale : true,
        origin: [ "US", "China" ]
      },

      {
        name : "Mango",
        color : "Yellow",
        stock : 10,
        price: 120,
        supplier_id : 2,
        onSale : false,
        origin: [ "Philippines", "India" ]
      }
    ]);

// Using the Aggregate Method
db.fruits.aggregate([
    // $match -> used to match or get documents that satisfies the condition, is similar to find()
    // $match -> Apple, Kiwi, Banana
    // $group -> allows us to group together documents and create an analysis output of the group elements
    // _id: $supplier_id
    /* 
        Apple = 1.0
        Kiwi = 1.0
        Banana = 2.0

        _id: 1.0
            Apple, Kiwi

            total: sum of the fruit stock of 1.0
            total: Apple stocks + Kiwi stocks
            total: 20 + 25
            total: 45

        _id: 2.0
            Banana

            total: Sum of the fruit stock of 2.0
            total: Banana stocks
            total: 15
    */
    // $sum - Used to add or total the values in a given field   
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
])

// Result from Aggregate method
/* {
  _id: 1,
  total: 45
}
{
  _id: 2,
  total: 15
} */

db.fruits.aggregate([
    // $match - Apple, Kiwi, Banana
    { $match: { onSale: true } },
    /* 
        Apple - 1.0
        Kiwi - 1.0
        Banana - 2.0

        _id: 1.0
            avgStocks: average stocks of fruits in 1.0
            avgStocks: (Apple Stocks + Kiwi Stocks) / 2
            avgStocks: (20            + 25        ) / 2
            avgStocks: 45 / 2
            avgStocks: 22.5

        _id: 2.0
            avgStocks: Average stocks of fruits in 2.0
            avgStocks: Banana Stocks / 1
            avgStocks: 15 / 1
            avgStocks: 15
    */
    /* 
        {
            _id: 1,
            avgStocks: 22.5
        }
        {
            _id: 2,
            avgStocks: 15
        }
    */
    // $avg - gets the average of the values of the given field per group
    { $group: { _id: "$supplier_id", avgStocks: { $avg: "$stock" } } },
    // $project - can be used when "aggregating" data to include/exclude fields from the returned results (Field Projection)
    /* 
        {
            avgStocks: 22.5
        }
        {
            avgStocks: 15
        }
    */
    { $project: { _id: 0 } }
])

db.fruits.aggregate([
    // $match: Apple, Kiwi, Banana
    { $match: { onSale: true } },
    /* 
        Apple - 1.0
        Kiwi - 1.0
        Banana - 2.0

        _id: 1.0
            maxPrice: Find the highest price of fruit
            maxPrice: Apple Price vs Kiwi Price
            maxPrice:  40 vs 50
            maxPrice: 50

        _id: 2.0
            maxPrice: Average stocks of fruits in 2
            maxPrice: Banana Price
            maxPrice: 20
            maxPrice: 20
    */
    /*
        $sort: maxPrice: -1 
        {
            _id: 1,
            maxPrice: 50
        }
        {
            _id: 2,
            maxPrice: 20
        }

        $sort: id: -1
        {
            _id: 2,
            maxPrice: 20
        }
        {
            _id: 1,
            maxPrice: 50
        }
    */
    { $group: { _id: "$supplier_id", maxPrice: { $max: "$price" } } },
    // $sort - change the order of the aggregated result
    // Providing a value of -1 will sort the aggregated results in a reverse order
    { $sort: { maxPrice: -1 } }
])

db.fruits.aggregate([
    { $unwind: "$origin" }
])

db.fruits.aggregate([
    { $unwind: "$origin" },
    { $group: {_id: "$origin", kinds: { $sum: 1 } } }
])
/* 
    {
        _id: 'India',
        kinds: 1
    }
    {
        _id: 'China',
        kinds: 1
    }
    {
        _id: 'US',
        kinds: 2
    }
    {
        _id: 'Philippines',
        kinds: 3
    }
    {
        _id: 'Ecuador',
        kinds: 1
    }
*/

db.fruits.aggregate([
    // $match - Apple, Kiwi, Banana
    { $match: { onSale: true } },
    /* 
        Apple - 1.0
        Kiwi - 1.0
        Banana - 2.0

        _id: 1.0
            avgStocks: average stocks of fruits in 1.0
            avgStocks: (Apple Stocks + Kiwi Stocks) / 2
            avgStocks: (20            + 25        ) / 2
            avgStocks: 45 / 2
            avgStocks: 22.5

        _id: 2.0
            avgStocks: Average stocks of fruits in 2.0
            avgStocks: Banana Stocks / 1
            avgStocks: 15 / 1
            avgStocks: 15
    */
    /* 
        {
            _id: 1,
            avgStocks: 22.5
        }
        {
            _id: 2,
            avgStocks: 15
        }
    */
    // $avg - gets the average of the values of the given field per group
    { $group: { _id: "$supplier_id", avgStocks: { $avg: "$stock" } } },
    // $project - can be used when "aggregating" data to include/exclude fields from the returned results (Field Projection)
    /* 
        {
            avgStocks: 22.5
        }
        {
            avgStocks: 15
        }
    */
    { $project: { _id: 0 } }
])


// fruitsOnSale
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $count: "fruitsOnSale" }
])

// fruitsInStock
db.fruits.aggregate([
    { $match: { onSale: true, stock: {$gte: 20} } },
    { $count: "enoughStock" }
])

// fruitsAvePrice
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", avg_price: { $avg: "$price" } } },
    { $sort: { _id: -1 } }
])

// fruitsHighPrice
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", max_price: { $max: "$price" } } },
    { $sort: { max_price: -1 } }
])

// fruitsLowPrice
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", min_price: { $min: "$price" } } },
    { $sort: { _id: -1 } }
])