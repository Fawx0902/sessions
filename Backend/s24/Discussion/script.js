// While Loop
let count = 5;

while (count !== 0) {
    console.log("Current value of count " + count);
    count--;
}

// Do-While Loop
let number = Number(prompt("Give me a number: "));

do{
    console.log("Current value of number: " + number);
    number += 1;
} while (number < 10);

// For Loop
for (let count = 0; count <= 20; count++){
    console.log("Current for loop value: " + count);
}

let my_string = "earl";

// Get length of the string
console.log(my_string.length);

// Get a specific letter in a string
console.log(my_string[0]);

// Loops through each letter in the string and will keep iterating as long as the current index is less that the length of the string.
for(let index = 0; index < my_string.length; index++) {
    console.log(my_string[index]); //Starts at letter 'e' and will keep printing each letter upto 'l'.
}

// Mini Activity (20 Mins.)
// 1. Loop through the 'my_name' variable which has a string with your name on it.
// 2. Display each letter in the console but exclude all the vowels from it.
let my_name = "Timothy Rey Cheong"
let vowels = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u'];
for (let index = 0; index < my_name.length; index++) {
    if (!vowels.includes(my_name[index])) {
        console.log(my_name[index]);
    }
}

let name_two = "rafael";

for (let index = 0; index < name_two.length; index++){
    // If the current letter is 'e' then it will skip the currnet loop.
    if(name_two[index].toLowerCase() == 'a'){
        console.log("Skipping...");
        continue;
    }
    // If the current letter is 'a' then it will break/stop the whole loop entirely.
    if(name_two == 'e'){
        break;
    }
}