console.log("ES6 Updates");

// Exponent Operators
const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

// Template Literals
// Allows us to write strings w/o using concantenation
// Helps us with code readability

// Using Concantenation
let name = "Ken"
let message = 'Hello ' + name + '! Welcome to Programming';
console.log('Message without template literals: ' + message);

// Using Template Literals
// Backticks (``) and ${} for including JavaScript Expressions

message = `Hello ${name}! Welcome to Programming!`;
console.log(`Message with Template Literals: ${message}`);

// Creates multi-line using template literals
const anotherMessage = `
${name} attended a Math Competition.
He won it by solving the proble 8 ** 2 with the answer of ${firstNum}`;
console.log(anotherMessage);

const interestRate = .1;
const principalAmount = 1000;

console.log(`The interest on your savings amount is ${principalAmount * interestRate}.`);

// Array Destructuring
const fullName = ["Juan", "Dela", "Cruz"];

// Using Array Indices
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`)


/* 
    const firstName = fullName[0];
    const middleName = fullName[1];
    const lastName = fullName[2];
*/
// Using Array Destructuring
const [firstName, middleName, lastName] = fullName;

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`)

// Object Destructuring
const person = {
    givenName: "Jane",
    maidenName: "Dela",
    familyName: "Cruz"
}

// Using dot notation
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you.`);

// Using Object Destructuring
const {givenName, familyName, maidenName} = person;

console.log(`Hello ${givenName} ${familyName} ${maidenName}! It's good to see you.`);

function getFullName({givenName, familyName, maidenName}) {

    console.log(`${givenName} ${maidenName} ${familyName}`);
};

getFullName(person);

// Arrow Functions
const hello = () => {
    console.log("Hello World!");
}

hello();

// Traditional Functions without Template Literals
// function printFullName (fName, mName, lName){
//     console.log(fName + ' ' + mName + ' ' + lName);
// }

// printFullName('John', 'D', 'Smith');

// Arrow Function with Template Literals
const printFullName = (fName, mName, lName) => {
    console.log(`${fName} ${mName} ${lName}`);
};

printFullName('Jane', 'D', 'Smith');

// Arrow Functions with Loops
const students = ['John', 'Jane', 'Judy'];

// Traditional Function
students.forEach(function(student){
    console.log(`${student} is a student.`);
})

// Arrow Function
students.forEach(student => {
    console.log(`${student} is a student.`);
})

// Implicit Return Statements 

// Traditional Function
// function add(x,y){
//     let result = x + y;
//     return result;
// }

// let total = add(2,5);
// console.log(total);

// Arrow Function
// Single Line Arrow Function
const add = (x, y) => x + y;

let total = add(2,5);
console.log(total);

// Default Argument Values
const greet =  (name = 'User') => {
    return `Good Afternoon ${name}`;
}

console.log(greet()); //Default name variable is User
console.log(greet('Judy'));

// Create a Class
class Car {
    constructor(brand, name, year){
        this.brand = brand;
        this.name = name;
        this.year = year;
    }
}

//Instantiate an Object
const fordExplorer = new Car();

// Even though the 'fordExplorer' is a constant object, since it is an Object, you may still re-assign values for its properties.
fordExplorer.brand = "Ford";
fordExplorer.name = "Explorer";
fordExplorer.year = 2022;

console.log(fordExplorer);

// This logic applies whether you re-assign the values of each property separately or put it as arguments of the new instance of the class.
const toyotaVios = new Car("Toyota", "Vios", 2018);
console.log(toyotaVios);
