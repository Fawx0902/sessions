console.log("Hi!");

// Get Post Data - Retrieve Data from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => {
    //console.log(data)
    showPosts(data)});

// console.log(document);
// console.log(document.querySelector('#form-add-post'));
// let textTitle = console.log(document.querySelector('#txt-title').value);
// console.log(textTitle);

//Add Post Data
document.querySelector('#form-add-post').addEventListener('submit', 
    // Prevent the default behavior of our Form element.
    (e) => { e.preventDefault();
    // Function Code Block or Task after triggering the event. 
            fetch('https://jsonplaceholder.typicode.com/posts', {
            method: "POST",
            body: JSON.stringify({
                title: document.querySelector('#txt-title').value,
                body: document.querySelector('#txt-body').value,
                userId: 1
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        alert('Post Successfully created!');

        document.querySelector('#txt-title').value = null;
        document.querySelector('#txt-body').value = null;
    })
});

// Show Posts - Display all the Post Objects from our JSON Placeholder API

const showPosts = (posts) => {
    let postEntries = '';

    posts.forEach((post) => {
        // JSX Syntax  - JavaScript Extended / Extension
        // ${`sample-id-variable`} Call on Variable on Object (Use Backtick [`], not Quote ['])
        postEntries += `
            <div id = "post-${post.id}">
                <h3 id = "post-title-${post.id}">${post.title}</h3>
                <p id = "post-body-${post.id}">${post.body}</p>
                <button onclick = "editPost(${post.id})">Edit</button>
                <button onclick = "deletePost(${post.id})">Delete</button>
            </div>
        `;
    })

    document.querySelector('#div-post-entries').innerHTML = postEntries;
};

// Edit Post Form
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
    document.querySelector('#btn-submit-update').removeAttribute('disabled');
}

// Update Button Function
// Update Post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert("Post updated successfully!")

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		document.querySelector('#btn-submit-update').setAttribute('disabled', true);

	})

});

const deletePost = (id) => {
    fetch('https://jsonplaceholder.typicode.com/posts')
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        data.filter((post) => {
            if (post.id === id){
                document.querySelector(`#post-${post.id}`).remove();
                alert('Post is deleted!');
            }
        })
    });
}

document.querySelector('#delete-all').addEventListener('click', () => {
    try {
        document.querySelector('#div-post-entries').remove();
        alert('All Posts are deleted!');
    }
    catch(error){
        alert('There are no posts to delete..');
    }
});