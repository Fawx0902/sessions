// Objects
// An object is a data type that is used to represent real world objects to create properties and methods/functionalities.
// Creating objects using initializers/object literals

let cellphone = {
    name: "Nokia 3210",
    manufactureDate: 1999
};

console.log("Result from creating objects using initializers/object literals: ");
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a constructor function
function Laptop(name, manufacturerDate) {
    this.name = name;
    this.manufactureDate = manufacturerDate;
}

// Multiple Instance of an object using the "new" keyword
// This method is called Instantiation
let laptop = new Laptop('Lenovo', 2008);
console.log("Result from creating objects using a constructor function: ");
console.log(laptop);

let laptop2 = new Laptop('Macbook Air', 2020);
console.log("Result from creating objects using a constructor function: ");
console.log(laptop2);

// Accessing Object Properties
// Using Square Bracket Notation
console.log("Result from square bracket notation: " + laptop2['name']);

// Using Dot Notation
console.log("Result from square bracket notation: " + laptop2.name);

// Access Array Objects
let array = [laptop, laptop2];

console.log(array[0]['name']);
console.log(array[0].name);

// Adding/Deleting/Reassigning Object Properties
// Empty Object
let car = {};

// Adding Object Properties using Dot Notation
// objectName.key = 'value'
car.name = "Honda Povic";
console.log("Result from adding properties using dot notation: ");
console.log(car)

// Adding Object Properties using Square Bracket Notation
car['Manufacturing Date'] = 2019;
console.log(car['Manufacturing Date']);
console.log(car['manufacturing date']);
// We cannot access the object properly using Dot Notation if the key has spaces
// console.log(car.Manufacturing Date);
console.log("Result from adding properties using Square Bracket Notation: ");
console.log(car);

// Deleting Object Properties
delete car['Manufacturing Date'];
// delete car.manufactureDate
console.log("Result from deleting properties: ")
console.log(car);

// Reassigning Object Properties
car.name = "Honda Civic Type R";
console.log("Result from reassigning properties: ")
console.log(car);

// Object Methods
// A Method is a function that acts as a property of an object

let person = {
    name: 'Barbie',
    greet: function(){
        console.log("Hello! My name is " + this.name);
    }
}

console.log(person);
console.log("Result from object methods: ");
person.greet();

// Adding Methods to Objects
person.walk = function() {
    console.log(this.name + " walked 25 steps forward!");
}

person.walk();

let friend = {
    name: "Ken",
    address: {
        city: "Austin",
        state: "Texas",
        country: "USA"
    },
    email: ['ken@gmail.com', 'ken@mail.com'],
    introduce: function(object) {
        console.log('Nice to meet you ' + object.name + "! I am " + this.name + ", from " + this.address.city + " " + this.address.state + " " + this.address.country + "!");
    }
}

friend.introduce(person);