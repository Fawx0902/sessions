const User = require('../models/User.js');
const bcrypt = require('bcrypt')
const auth = require('../auth.js');

module.exports.registerUser = async (request, response) => {

    const isDuplicateEmail = await User.findOne({email: request.body.email}).select('email')
    
    if (isDuplicateEmail.email === request.body.email) {
        return response.send('Email already exists.')
    }

    let new_user = new User ({
        email: request.body.email,
        password: bcrypt.hashSync(request.body.password, 10)
    });

    return new_user.save().then((registered_user, error) => {
        if (error){
            return response.send({
                message: error.message
            })
        }

        return response.send({
            message: 'Successfully registered a user!',
            data: registered_user
        })
    }).catch(error => console.log(error))
}

module.exports.loginUser = (request, response) => {
    return User.findOne({email: request.body.email}).then((result) => {
        if (result == null){
            return response.send({
                message: "The user isn't registered yet."
            })
        }

        // If a user was found with an existing email, then check if the password of that user matched the input from the request body
        const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

        if(isPasswordCorrect){
            // If the password comparison returns true, then respond with the newly generated JWT access token.
            return response.send({accessToken: auth.createAccessToken(result)});
        } else {
            return response.send({
                message: "Your password is incorrect."
            })
        }
    }).catch(error => response.send(error))
}

module.exports.getUserProfile = (request, response) => {
    return User.findOne({_id: request.user.id}).then((result, error) => {
        if (error) {
        return { message: error.message };
    } 
        result.password = ""
        return response.send(result);
    })
}

module.exports.permitAdmin = (request, response) => {
    return User.findByIdAndUpdate(request.body.id, {isAdmin: true}).then(() => {
        return response.send({
            message: 'Succesfully bestowed user with Admin privileges.'
        });
    })
    .catch(error => {
        return response.send({
            message: 'An error occurred while granting someone admin.',
            error: error.message
        });
    })
}