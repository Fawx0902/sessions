const Order = require('../models/Order.js');
const Product = require('../models/Product.js');

module.exports.checkoutOrder = async (request, response) => {
    // Blocks this function if user is admin.
    if(request.user.isAdmin){
        return response.send('Action Forbidden')
    }

    const product = await Product.findOne({_id: request.body.productId, isActive: true}).select('price')

    if (!product) {
        return response.send('Product is unavailable.')
    }

    const productName = await Product.findOne({_id: request.body.productId}).select('name')

    let new_Order = new Order({
        userId: request.body.userId,
        products: [{
            productName: productName.name,
            productId: request.body.productId,
            quantity: request.body.quantity,
        }],
        totalAmount: product.price * request.body.quantity
    })

    return new_Order.save().then((register_order, error) => {
        if (error){
            return respond.send({
                message: error.message
            })
        }
        
        return response.send({
            message: 'Successfully checkout Order!',
            data: register_order
        })
    })
}

module.exports.viewOrders = (request, response) => {
    // Blocks this function if user is admin.
    if(request.user.isAdmin){
        return response.send('Action Forbidden')
    }

    return Order.find({userId: request.user.id}).then(result => {
        return response.send(result);
    })
}

module.exports.viewAllOrders = (request, response) => {
    return Order.find({}).then(result => {
        return response.send(result);
    })
}