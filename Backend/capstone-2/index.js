// Server Variables
const express = require('express');
const mongoose =  require('mongoose');
const cors = require('cors');

require('dotenv').config();

const port = 4000;
const userRoutes = require('./routes/userRoutes.js')
const productRoutes = require('./routes/productRoutes.js')
const orderRoutes = require('./routes/orderRoutes.js')
const app = express();

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors()); // This will allow our hosted front-end app to send requests to this server

// Routes
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);
app.use('/api/orders', orderRoutes);

// Database Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@303-cheong.oozpdie.mongodb.net/b303-ecommerce-api?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let database = mongoose.connection;

database.on('error', () => console.log('Connection error :('));
database.once('open', () => console.log('Connected to MongoDB!'));

app.listen(process.env.PORT || port, () => {
    console.log(`Booking System API is now running at localhost:${process.env.PORT || port}`);
});

module.exports = app;
