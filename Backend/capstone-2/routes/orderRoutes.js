const express = require('express');
const router = express.Router();
const OrderController = require('../controllers/OrderControllers.js')
const auth = require('../auth.js')
const {verify, verifyAdmin} = auth

// Let Non-admin user checkout (Create Order)
router.post('/checkout', verify, (request, response) => {
    OrderController.checkoutOrder(request, response)
})

// Retrieve authenticated user’s orders
router.get('/', verify, (request, response) => {
    OrderController.viewOrders(request, response)
})

// Retrieve all orders (Admin only)
router.get('/all', verify, verifyAdmin, (request, response) => {
    OrderController.viewAllOrders(request, response)
})


module.exports = router;