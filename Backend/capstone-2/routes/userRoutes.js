const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserControllers.js')
const auth = require('../auth.js')
const {verify, verifyAdmin} = auth


// Register a new User
router.post('/register', (request, response) => {
    UserController.registerUser(request, response);
})

// Login User
router.post('/login', (request, response) => {
    UserController.loginUser(request, response);
})

// Retrieve User Details
router.get('/details', verify, (request, response) => {
	UserController.getUserProfile(request, response);
})

// Set another user as Admin (Admin Only)
router.put('/setadmin', verify, verifyAdmin, (request, response) => {
    UserController.permitAdmin(request, response);
})



module.exports = router;