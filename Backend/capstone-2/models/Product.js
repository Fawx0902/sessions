const mongoose = require('mongoose');

// Schema
const product_schema = new mongoose.Schema({
    name : {
        type : String,
        required : [true, "Name of product is required!"]
    },
    description : {
        type : String,
        required : [true, "Description is required!"]
    },
    price : {
        type : Number, 
        required : [true, "Price of Product is required!"]
    },
    isActive : {
        type : Boolean, 
        default : true
    },
    createdOn: {
        type : Date,
        default : new Date() 
    }
})



module.exports = mongoose.model("Product", product_schema);