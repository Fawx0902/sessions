const mongoose = require('mongoose');

// Schema
const order_schema = new mongoose.Schema({
    userId : {
        type : String,
        required : [true, "userId is required!"]
    },
    products : [
        {
            productName : {
                type : String,
                required: [true, "productId is required!"]
            },

            productId : {
                type : String,
                required: [true, "productId is required!"]
            },

            quantity : {
                type : Number,
                required: [true, "Quantity of product is required!"]
            }
        }
    ],
    totalAmount : {
        type : Number, 
        required: [true, "Total Amount is required!"]
    },
    purchasedOn: {
        type : Date,
        default : new Date() 
    }
})



module.exports = mongoose.model("Order", order_schema);