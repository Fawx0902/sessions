// Function Declaration and Invocation
function printName(){
    console.log("My name is Jeff.");
};

printName();

// Function Expression
let variable_function = function(){
    console.log("Hello from function expression!");
};

variable_function();

// Scoping
let global_variable = "Call me Mr. Worldwide!";

console.log(global_variable);

function showNames(){
    let function_variable = "Joe";
    const function_const = "John";

    console.log(function_variable);
    console.log(function_const);

    // Can use Global Variables inside any function as long as they are declared outside the function scope.
    console.log(global_variable);
};

// Cannot use locally-scoped variables outside the function they are declared in.
// console.log(function_variable);

showNames();

// Nested Functions
function parentFunction(){
    let name = "Jane";

    function childFunction(){
        let nested_name = "John";
        // Accessing the 'nested_name' variable within the same function it was declared in, will work.
        console.log(name);
        console.log(nested_name);
    };
    // Accessing the 'nested_name' variable outside the function it was declared in, will not work.
    // console.log(nested_name);
    childFunction();
};

parentFunction();

// Best Practice for Function Naming
function printWelcomeMessageForUser(){
    let first_name = prompt("Enter your first name: ");
    let last_name = prompt("Enter your last name: ");

    console.log("Hello, " + first_name + " " + last_name + "!");
    console.log("Welcome sa page ko!");
};

printWelcomeMessageForUser();

// Return Statement
function fullName(){
    return "Earl Rafael Diaz";
}

console.log(fullName());