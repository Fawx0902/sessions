console.log("Array Mutator Methods");

// Iteration Methods (Mutator) (Non-Mutator)
// Loops through all the elements to perform repetitive tasks on the array

// forEach() - to loop through the array
// map() - loops through the array and returns a new array
// filter() - returns a new array containing elements which meets the given condition

// every() - checks if all elements meet the given condition
// return true f all elements meet the condition, otherwise false.
let numbers = [1, 2, 3, 4, 5, 6];

let allValid = numbers.every(function(number) {
    return number > 3;
})

console.log("Result of every() method: ");
console.log(allValid);

// some() - checks if at least one element meets the given condition
// returns true if atleast one element meets the given condition, otherwise false.
let someValid = numbers.some(function(number) {
    return number < 2;
})

console.log("Result of some() method: ");
console.log(someValid);

// includes() method - methods can be "chained" using them one after another
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product){
    return product.toLowerCase().includes('a');
})

console.log(filteredProducts)

// reduce()
let iteration = 0;

let reducedArray = numbers.reduce(function(x,y){
    console.warn("Current Iteration " + ++iteration);
    console.log("Accumulator: " + x);
    console.log("currentValue: " + y);

    return x + y
})

console.log("Result of reduce() method: " + reducedArray);

let productsReduce = products.reduce(function(x,y){
    
    return x + ' ' + y
})

console.log("Result of reduce() method: " + productsReduce)