db.users.insertMany([
 						{
                firstName: "Jane",
                lastName: "Doe",
                age: 21,
                contact: {
                    phone: "87654321",
                    email: "janedoe@gmail.com"
                },
                courses: [ "CSS", "Javascript", "Python" ],
                department: "none"
						},
            {
                firstName: "Stephen",
                lastName: "Hawking",
                age: 76,
                contact: {
                    phone: "87654321",
                    email: "stephenhawking@gmail.com"
                },
                courses: [ "Python", "React", "PHP" ],
                department: "none"
            },
            {
                firstName: "Neil",
                lastName: "Armstrong",
                age: 82,
                contact: {
                    phone: "87654321",
                    email: "neilarmstrong@gmail.com"
                },
                courses: [ "React", "Laravel", "Sass" ],
                department: "none"
            }
]);

// Greater Than Operator
db.users.find({
    age: {
        $gt: 20
    }
})

// Lesser Than Operator
db.users.find({
    age: {
        $lt: 80
    }
})

// Regex Operator
db.users.find({
    firstName: {
        $regex: 's',
        $options: 'i'
    }
})

db.users.find({
    firstName: {
        $regex: 't',
    }
})

// Combining Operators
db.users.find({
    age: {
        $gt: 70
    },
    lastName: {
        $regex: ''
    }
})

db.users.find({
    age: {
        $lte: 76
    },
    firstName: {
        $regex: 'j',
        $options: 'i'
    }
})

// Field Protection
db.users.find({}, {
    "_id": 0
})

db.users.find({}, {
    "_id": 0,
    "firstName": 1
})

db.users.find({
    $and: [
        { age: {$lte: 30} },
        { lastName: {$regex: 'e'} }
    ]
})

db.users.find({
    $or:[
          {firstName:{$regex:'s'}},
          {lastName: {$regex:'d'}}
        ]
  },
  {
    _id: 0,
    firstName:1,
    lastName:1
  })