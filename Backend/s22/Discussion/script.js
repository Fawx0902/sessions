// Arguments and Parameters
function printName(name) {
    console.log("I'm the real " + name + "!");
}

printName("Slim Shady");

function checkDivisibilityByTwo(number) {
    let result = number % 2;

    console.log("The remainder of " + number + " is " + result);
}

checkDivisibilityByTwo(10);
checkDivisibilityByTwo(5);

// Multiple Arguments and Parameters
function createFullname(firstName, middleName, lastName){
    console.log(firstName + " " + middleName + " " + lastName);
}

createFullname("Juan", "Dela", "Cruz");

// Usage of Prompts and Alerts
let user_name = prompt("Enter your username: ");

function displayWelcomeMessageForUser(userName){
    alert("Welcome back to Valorant " + userName + "!");
}

displayWelcomeMessageForUser(user_name);