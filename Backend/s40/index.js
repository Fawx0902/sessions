// Server variables for initialization
const express = require('express');
const app = express(); // Initialize Express
const port = 4000;

// Middleware
app.use(express.json()); //Registering a middleware that will make express be able to read JSON format from requests
app.use(express.urlencoded({extended: true})); // Middleware that will allow express to be able to to read data types other that the default string and array it can usually read.

// Server Listening
app.listen(port, () => console.log(`Server is running at port ${port}`));

// Routes
app.get('/', (request, response) => {
    response.send("Hello World!");
})

app.post('/greeting', (request, response) => {
    response.send(`Hello there ${request.body.firstName} ${request.body.lastName}`);
})

// Mock Database
let users = [];

app.post('/register', (request, response) => {
    if(request.body.username !== '' && request.body.Password !== ''){
        users.push(request.body);

        response.send(`User ${request.body.username} has successfully been registered!`);
    } else {
        response.send("Please input BOTH username AND password.");
    }
})

app.get('/users', (request, response) => {
    response.send(users);
})


module.exports = app;