const Course = require('../models/Course.js');

module.exports.addCourse = (request_body) => {

    let newCourse = new Course({
        name: request_body.name,
        description: request_body.description,
        price: request_body.price
    })

    return newCourse.save().then((registered_course, error) =>{
        if (error){
            return {
                message: error.message
            }
        }
        return true
    }).catch(error => console.log(error))
}

module.exports.getAllCourses = (request, response) => {
    return Course.find({}).then(result => {
        return response.send(result)
    })
}

module.exports.getAllActiveCourses = (request, response) => {
    return Course.find({isActive: true}).then(result => {
        return response.send(result)
    })
}

module.exports.getCourse = (request, response) => {
    return Course.findById({_id: request.params.id}).then(result => {
        return response.send(result);
    })
}

module.exports.updateCourse = (request, response) => {
    let updated_course_details = {
        name: request.body.name,
        description: request.body.description,
        price: request.body.price
    }

    return Course.findByIdAndUpdate(request.params.id, updated_course_details).then((error) => {
        if(error) {
            return response.send({
                message: error.message
            })
        }

        return response.send({
            message: 'Course has been updated successfully.'
        })
    })
}

module.exports.archiveCourse = (request,response) => {
    return Course.findByIdAndUpdate(request.params.id, {isActive: false}).then((error) =>{
        if(error){
            return response.send({
            message: error.message
            })
        }

        return response.send(true)
    })
}

module.exports.activateCourse = (request, response) => {
    return Course.findByIdAndUpdate(request.params.id, {isActive: false}).then((error) =>{
        if (error) {
            return response.send({
                message: error.message
            })
        }

        return response.send(true);
    })
}

module.exports.searchCourse = async (request, response) => {
    const courseName = request.body.courseName;

    return Course.find({ name: {$regex: courseName, $options: 'i'}}).then((courses) => {
        response.send(courses)
    }).catch(error => response.send({
        message: error.message
    }))
}

module.exports.deleteCourse = (request, response) => {
    const courseId = request.params.id;

    Course.findByIdAndDelete(courseId)
        .then((deletedCourse) => {
            if (!deletedCourse) {
                return response.status(404).json({ message: 'Course not found' });
            }
            return response.status(200).json({ message: 'Course deleted successfully' });
        })
        .catch((error) => {
            console.error('Error deleting course:', error);
            return response.status(500).json({ message: 'Internal server error' });
        });
};
