const User = require('../models/User.js');
const Course = require('../models/Course.js');
const bcrypt = require('bcrypt')
const auth = require('../auth.js');

module.exports.checkEmailExists = (request_body) => {
    // Insert Logic Here
    return User.find({email: request_body.email}).then((result, error) => {
        if (error){
            return {
                message: error.message
            }
        }

        if (result.length <= 0){
            return false;
        }

        return true
    })
}

module.exports.registerUser = (request_body) => {
    
    let new_user = new User ({
        firstName: request_body.firstName,
        lastName: request_body.lastName,
        email: request_body.email,
        mobileNo: request_body.mobileNo,
        password: bcrypt.hashSync(request_body.password, 10)
    });

    return new_user.save().then((registered_user, error) => {
        if (error){
            return {
                message: error.message
            }
        }

        return {
            message: 'Successfully registered a user!',
            data: registered_user
        }
    }).catch(error => console.log(error))
}

module.exports.loginUser = (request, response) => {
    return User.findOne({email: request.body.email}).then((result) => {
        if (result == null){
            return response.send({
                message: "The user isn't registered yet."
            })
        }

        // If a user was found with an existing email, then check if the password of that user matched the input from the request body
        const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

        if(isPasswordCorrect){
            // If the password comparison returns true, then respond with the newly generated JWT access token.
            return response.send({accessToken: auth.createAccessToken(result)});
        } else {
            return response.send({
                message: "Your password is incorrect."
            })
        }
    }).catch(error => response.send(error))
}

module.exports.getProfile = (request_body) => {
    return User.findOne({_id: request_body.id}).then((result, error) => {
            if (error) {
            return { message: error.message };
        } 

        result.password = ""
        return result;
    })
}

module.exports.enroll = async(request, response) => {
    // Blocks this function if user is admin.
    if(request.user.isAdmin){
        return response.send('Action Forbidden')
    }

    // This variable will return true once the user data has been updated.
    let isUserUpdated = await User.findById(request.user.id).then(user => {
        let new_enrollments = {
            courseId: request.body.courseId
            // courseName: request.body.courseName,
            // courseDescription: request.body.courseDescription,
            // coursePrice: request.body.coursePrice
        }

        user.enrollments.push(new_enrollments);

        return user.save().then(user => true).catch(error => error.message);
    })

    // Sends any error within this 'isUserUpdated' as a response.
    if (isUserUpdated !== true){
        return response.send({message: isUserUpdated})
    }

    // Updating Course Collection
    let isCourseUpdated = await Course.findById(request.body.courseId).then(course => {
        let new_enrollee = {
            userId: request.user.id
        }

        course.enrollees.push(new_enrollee)

        return course.save().then(updated_course => true).catch(error => error.message);
    })

    // Sends any error within this 'isCourseUpdated' as a response.
    if (isCourseUpdated !== true){
        return response.send({message: isCourseUpdated})
    }

    // Once isUserUpdated AND isCourseUpdated return true
    if (isUserUpdated && isCourseUpdated) {
        return response.send({message: "Enrolled Successfully!"})
    }
}

module.exports.getEnrollments = (request, response) => {
    User.findById(request.user.id)
        .then(result => response.send(result.enrollments))
        .then(error => response.send(error.message))
}

