const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserControllers.js')
const auth = require('../auth.js')

// You can destructure the 'auth' variable to extract the function being exported from it. You can then use the functions directly without having to use dot (.) notation.
const {verify, verifyAdmin} = auth

// Check if email exists
router.post('/check-email', (request, response) => {
    UserController.checkEmailExists(request.body).then((result => {
        response.send(result);
    }))
})

// Register a new User
router.post('/register', (request, response) => {
    UserController.registerUser(request.body).then((result => {
        response.send(result);
    }))
})

// Login User
router.post('/login', (request, response) => {
    UserController.loginUser(request, response);
})

// Show details of User if they're admin
router.post('/details', verify, verifyAdmin, (request, response) => {
    UserController.getProfile(request.body).then((result) => {
        response.send(result);
    });
})

// Enroll user to a course
router.post('/enroll', verify, (request, response) => {
    UserController.enroll(request, response);
})

// Get user enrollments
router.post('/enrollments', verify, (request, response) => {
    UserController.getEnrollments(request, response);
})

module.exports = router;