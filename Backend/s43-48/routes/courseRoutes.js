const express = require('express');
const router = express.Router();
const CourseController = require('../controllers/CourseControllers.js')
const auth = require('../auth.js')

const {verify, verifyAdmin} = auth

// Create course if User is admin
router.post('/', verify, verifyAdmin, (request, response) => {
    CourseController.addCourse(request.body).then((result) => {
        response.send(result);
    })
})

// Get all Courses
router.get('/all', (request, response) => {
    CourseController.getAllCourses(request, response);
})

// Get Active Courses
router.get('/', (request, response) => {
    CourseController.getAllActiveCourses(request, response);
})

// Get Single Course
router.get('/:id', (request, response) => {
    CourseController.getCourse(request, response)
})

// Update Course
router.put('/:id', verify, verifyAdmin, (request, response) => {
    CourseController.updateCourse(request, response)
})

// Deactivate Course
router.put('/:id/archive', verify, verifyAdmin, (request, response) => {
    CourseController.archiveCourse(request, response)
})

// Activate Course
router.put('/:id/activate', verify, verifyAdmin, (request, response) => {
    CourseController.activateCourse(request, response);
})

// Search Courses by Course Name
router.post('/search', (request, response) => {
    CourseController.searchCourse(request, response);
})

// Delete Course
router.delete('/:id', verify, verifyAdmin, (request, response) => {
    CourseController.deleteCourse(request, response);
});

module.exports = router;