// JSON Example
// {
//     "city": "Pateros",
//     "province": "Metro Manila";
//     "country": "Philippines"
// }

// JSON Arrays
// "cities": [
//     {
//         "city": "Quezon City",
//         "province": "Metro Manila",
//         "country": "Philippines"
//     },

//     {
//         "city": "Batangas City",
//         "province": "Batangas",
//         "country": "Philippines"
//     },

//     {
//         "city": "Star City",
//         "province": "Pasay",
//         "country": "Philippines",
//         "rides": [
//             {
//                 "name": "Star Flyer"
//             },
//             {
//                 "name": "Gabi ng Lagim"
//             }
//         ]
//     }
// ]

// JSON Methods
let zuitt_batches = [
    { batchName: "303" },
    { batchName: "271" },
]

// Before Stringification, javascript reads the variable as a regular JS Array
console.log("Output before Stringification: ");
console.log(zuitt_batches);

// After JSON.Stringify, Javascript now reads the variable as a string (Equivalent to covnverting an array into JSON Formula)
console.log("Output after Stringification: ");
console.log(JSON.stringify(zuitt_batches));

// User Details
let first_name = prompt("What is your First Name?");
let last_name = prompt("What is your Last Name?");

let other_data = JSON.stringify({
    firstName: first_name,
    lastName: last_name
})

console.log(other_data);

// Convert Stringified JSON into Javascript Objects
let other_data_JSON = `[{ "firstName": "Earl", "lastName": "Diaz" }]`;

let parsed_other_data = JSON.parse(other_data_JSON);

console.log(parsed_other_data);
console.log(parsed_other_data[0].firstName)
