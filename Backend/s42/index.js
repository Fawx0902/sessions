// Server variables 
const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config() //Initialization of dotenv package
const taskRoutes = require('./routes/taskroutes.js')
const app = express(); 
const port = 4000;

// MongoDB Connection
// mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@303-cheong.oozpdie.mongodb.net/<database-name>?retryWrites=true&w=majority
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@303-cheong.oozpdie.mongodb.net/b303-todo?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let database = mongoose.connection;

database.on('error', () => console.log('Connection error :('));
database.once('open', () => console.log('Connected to MongoDB!'));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true})); 
app.use('/api/tasks', taskRoutes) // Initializing the routes for '/tasks' so the server will know the routes aailable to send requests to.


// Server Listening
app.listen(port, () => console.log(`Server is running at port ${port}`))

module.exports = app