const express = require('express');
const router = express.Router();
const TaskController = require('../controllers/TaskController.js')

// Insert Routes Here


router.post('/', (request, response) => {
	// MINI ACTIVITY (15 mins.) until 2:25PM
	// -Use the proper function from the TaskController that will create a new task in the database.
	// -Make sure to pass the request body as an argument so the function will be able to use the 'name' property.
	// Type 'd' in GMeet once done.
	TaskController.createTask(request.body).then(result => {
		response.send(result); 
	})
})

// Get all Tasks
router.get('/', (request, response) => {
    TaskController.getAllTasks().then(result => {
        response.send(result);
    })
})


module.exports = router;