console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// The fetch() function returns a Promise which can then be chained using the then() function. The then() function waits for the promise to be resolved before executing code.
fetch('https://jsonplaceholder.typicode.com/posts')
    .then(response => response.json()) // Converts the JSON string from the response into regular javascript format
    .then(posts => console.log(posts));

// As of ES6, the async/await syntax can be used to replace the .then() convention as it helps a function to be asynchronous and accomplished the same output as the .then syntax.
async function fetchData(){
    let result = await fetch('https://jsonplaceholder.typicode.com/posts')

    let json_result = await result.json()

    console.log(json_result);
}

fetchData();

// Adding Headers, Body and Method to the fetch() function.
// Creating new posts   
fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: "New Post!",
        body: "Hello World!",
        userId: 2
    })
})
.then(response => response.json())
.then(created_post => console.log(created_post))

// Updating existing posts
fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: "Corrected Post!",
    })
})
.then(response => response.json())
.then(updated_post => console.log(updated_post))

// Deleting Existing Post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'DELETE'
})
.then(response => response.json())
.then(deleted_post => console.log(deleted_post))

// Filtering Posts
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(response => response.json())
.then(filtered_post => console.log(filtered_post))

// Getting Comments of a Posts
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then(response => response.json())
.then(comments => console.log(comments))