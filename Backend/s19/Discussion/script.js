// JavaScript consists of whats called "statements". And statements are basically just syntax with semi colons at the end.
// alert("Hello World!");

// JavaScript can access the 'log' function of the console to display text/data in the console.
console.log("Hello World!")

// [SECTION] Variables

// Variable Declaration & Invocation
let my_variable = "Hola, Mundo!";
console.log(my_variable);

// Concatenating Strings
let country = "Philippines";
let province = "Metro Manila";

let full_address = province + ', ' + country;
console.log(full_address)

// Numbers
let headcount = 26;
let grade = 98.7;

console.log("The number of students is " + headcount + " and the average grade of all students is " + grade);

let sum2 = headcount + grade

console.log(sum2)

// Boolean
let is_married = false;
let is_good_conduct = true;

console.log("He's married: " + is_married);
console.log("She's a good person: " + is_good_conduct);

// Arrays
let grades = [98.7, 89.9, 90.2, 94.6];
let details = ["John", "Smith", 32, true];

console.log(details);

// Objects
let Person = {
    fullName: "Juan Dela Cruz",
    age: 40,
    isMarried: false,
    contact: ["09992223131", "09448876691"],
    address: {
        houseNumber: "345",
        city: "England"
    }
};

// JavaScript reads arrays as objects. This is mainly to accomodate for specific functionalities that arrays can do later on. 
console.log(typeof Person);
console.log(typeof grades);

// Null & Undefined Values
let girlfriend = null;
let full_name;

console.log(girlfriend);
console.log(full_name);

// [SECTION] Operators
// Arithmetic Operators
let first_number = 5;
let second_number = 5;

let sum =  first_number + second_number;
let difference = first_number - second_number;
let product = first_number * second_number;
let quotient = first_number / second_number;
let remainder = first_number % second_number;

console.log("Sum: " + sum);
console.log("Difference: " + difference);
console.log("Product: " + product);
console.log("Quotient: " + quotient);
console.log("Remainder: " + remainder);

// Assignment Operators
let assignment_number = 0;
assignment_number = assignment_number + 2;
console.log("Result of Addition Assignment Operator: " + assignment_number);

assignment_number += 2;
console.log("Result of Shorthand Addition Assignment Operator: " + assignment_number);