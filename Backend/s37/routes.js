let http = require('http');

// Create a variable port to store the port number: 4000
const port = 4000;

// Create a variable app that stores the output of the createServer() method
// This allows us to use the http createServer's other methods
const app = http.createServer((req, res) => {

    // req is an object that I sent via the browser
    // url is a property that refers to the urlor link in the browser
    if(req.url == '/greeting') {

        res.writeHead(200, {'Content-Type': 'text/plain'});

        res.end('Hello Again!');

    } else if (req.url == '/homepage'){

        res.writeHead(200, {'Content-Type': 'text/plain'});

        res.end('Welcome to the homepage!');

    } else {

        res.writeHead(404, {'Content-Type': 'text/plain'});

        res.end('Page not available..!');
    } //All other routes that are not included in if else-if

})

app.listen(port);

console.log(`Server is now accessible at localhost:${port}.`);

/* 
    {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        res.end('Page not available..!');
    }
*/