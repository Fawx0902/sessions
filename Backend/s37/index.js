// Use 'require' directive to load the Node.js modules
// Module is a software component or part of a program that contains one or more routines
// The 'http' module let Node.js transfer data using the HYPER TEXT TRANSFER PROTOCOL
// HTTP is a protocol that allows the fetching of resources like HTML Documents
let http = require("http");

// createServer() method creates an HTTP server that listens to requests on a specific port and gives response back to the client
// The http module has a createServer() method that accepts a function as an argument and allows for a creation of a server
// The arguments passed in the function are request and response objects (data type) that contains methods that allow us to receive requests from the client and send responses back to it
// 4000 is the port where the server will listen to. A port is a virtual point where the network connection starts and end.

http.createServer(function(request, response){

    // 200 is the status code for the response - means OK
    // sets the content-type to be plain text
    response.writeHead(200, {'Content-Type': 'text/plain'});

    // Sends the response with text context "Hello World!"
    response.end("Hello World!");

}).listen(4000)

// When the server is runnning,
console.log("Server is running at port 4000")