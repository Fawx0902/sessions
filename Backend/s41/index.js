// Server variables 
const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config() //Initialization of dotenv package
const app = express(); 
const port = 4000;

// MongoDB Connection
// mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@303-cheong.oozpdie.mongodb.net/<database-name>?retryWrites=true&w=majority
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@303-cheong.oozpdie.mongodb.net/b303-todo?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.on('error', (   ) => console.log('Connection Error :('));
mongoose.connection.once('open', () => console.log('Connected to MongoDB!'));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true})); 

// Mongoose Schema
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending"
    }
});

// Models
const Task =  mongoose.model("Task", taskSchema);

// Routes
// Creating a new Task
app.post('/tasks', (request, response) => {
    Task.findOne({name: request.body.name}).then((result, error) => {

        if (result != null && result.name == request.body.name){

            return response.send("Duplicate Task Found")
        } else {

            let newTask = new Task({
                name: request.body.name
            });

            newTask.save().then((savedTask, error) => {

                if (error){
                    return response.send({
                        message: error.message
                    });
                }

                return response.send(201, 'New Task Created!');
            })
        }
    })
})

// Get all Tasks
app.get('/tasks', (request, response) => {
    Task.find({}).then((result, error) => {
        if (error){
            return response.send({
                message: error.message
            });
        }

        return response.status(200).json({
            tasks: result
        });
    })
})


// Server Listening
app.listen(port, () => console.log(`Server is running at port ${port}`))

module.exports = app