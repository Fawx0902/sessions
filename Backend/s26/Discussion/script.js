// Arrays and Indexes
let grades = [98.5, 94.3, 89.2, 90.1];
let computer_brands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu", "Lenovo"];
let mixed_array = [12, "Asus", null, undefined, {}];

// Alternative way to write Arrays
let my_tasks = [
    "Drink HTML",
    "Eat Javascript",
    "Inhale CSS",
    "Bake Sass",
];

// Reassigning Values
console.log("Array before Reassignment: ");
console.log(my_tasks);

my_tasks[0] = "Run Hello World!"; // To reassign a vaue in an array, just use its index number and use an assignment operator to replace the value of that index.
console.log("Array after Reassignment: ")
console.log(my_tasks);

// Reading Arrays
console.log(computer_brands[1]);
console.log(grades[3]);

// Getting length of an array
console.log(computer_brands.length)

let index_of_last_element = computer_brands.length;

console.log(computer_brands[index_of_last_element - 1]);

// Array Methods

let fruits = ["Apple", "Orange", "Kiwi", "PassionFruit"];

console.log("Current Array: ");
console.log(fruits);

fruits.push("Mango", "Cocolemon");

console.log("Updated Array after Push Method: ");
console.log(fruits);

let removed_item = fruits.pop();

console.log("Updated Array after Pop Method: ");
console.log(fruits);
console.log("Removed Fruit: " + removed_item);

// Unshift Method - Add Items in beginning of an Array
console.log("Current Array: ");
console.log(fruits);

fruits.unshift("Lime", "Star Apple");
console.log("Updated Array after Unshift Method: ");
console.log(fruits);

// Shift Method - Pop Items in beginning of an Array
console.log("Current Array: ");
console.log(fruits);

fruits.shift();
console.log("Updated Array after Shift Method: ");
console.log(fruits);

// Splice Method - Pop Items in beginning of an Array
console.log("Current Array: ");
console.log(fruits);

fruits.splice(1, 2, 'Lime', 'Cherry');
console.log("Updated Array after Splice Method: ");
console.log(fruits);

// Sort Method - Sort Items in an Array
console.log("Current Array: ");
console.log(fruits);

fruits.sort(); // Sort in alphabetical order
// fruits.reverse(); // Sort the items in reverse alphabetical order
console.log("Updated Array after Sort Method: ");
console.log(fruits);

// Non-Mutator Methods - Every method written above is called a 'Mutator Method' because it modifies the value of the array one way or another. Non-mutator methods on the other hand, don't do the same thing, they instead execute specific functionalities that can be done with the existing array values.
// indexOf Method - gets the index of a specific item.
let index_of_lenovo = computer_brands.indexOf("Lenovo");
console.log("The index of Lenovo is: " + index_of_lenovo);

// lastIndexOf Method - gets the index of a specific item starting from the end of the array. This will only work if there are multiple instances of the same item. The item closest to the end of the array will be the one that the method will get the index of.
let index_of_lenovo_from_last_item = computer_brands.lastIndexOf("Lenovo");
console.log("The index of Lenovo starting from the end of the array is: " + index_of_lenovo_from_last_item);

// Slice Method
let hobbies = ['Gaming', 'Running', 'Gaslighting', 'Cycling', 'Writing'];

// By putting '2' as the argument, we are starting the slicing process from the item with the index of 2
let sliced_array_from_hobbies = hobbies.slice(2);
console.log(sliced_array_from_hobbies);
console.log(hobbies);

// By putting two arguments instead of 1, we are also specifying where the slice will end.
let sliced_array_from_hobbies_B = hobbies.slice(2,4);
console.log(sliced_array_from_hobbies_B);
console.log(hobbies);

// By using a negative number as the index, the count where it will start with will come from the end of the array instead of the beginning.
let sliced_array_from_hobbies_C = hobbies.slice(-2);
console.log(sliced_array_from_hobbies_C);
console.log(hobbies);

// toString Method
let string_Array = hobbies.toString();
console.log(string_Array);

// Concat method
let greeting = ["Hello", "World"];
let exclamation = ["!", "?"];

let concat_greeting = greeting.concat(exclamation);
console.log(concat_greeting);

// Join Method - This will convert the array to a string AND insert a specified separator between them. The separator is defined as the argument of the join() function.
console.log(hobbies.join(' - '));

// foreach Method
hobbies.forEach(function(hobby){
    console.log(hobby);
})

// Map Method - Loops throughout the whole array and adds each item to a new array.
let numbers_list = [1, 2, 3, 4, 5];

let numbers_map = numbers_list.map(function(number){
    return number * 2;
});

console.log(numbers_map);

// Filter Method
let filtered_numbers = numbers_list.filter(function(number){
    return (number < 3);
});
console.log(filtered_numbers);

// Multi-Dimensional Arrays
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard[3][2]);