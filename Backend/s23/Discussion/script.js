// If Else Statements

let number = 1;

if (number > 1){
    console.log("The number is greater than 1!");
} else if (number < 1) {
    console.log("The number is less than 1!");
} else {
    console.log("None of the conditions were true 😔")
}

// Falsy Values
if (false){
    console.log("Falsy!")
}

if (0){
    console.log("Falsy!")
}

if (""){
    console.log("Falsy!")
}

if (undefined){
    console.log("Falsy!")
}

if (null){
    console.log("Falsy!")
}

if (NaN){
    console.log("Falsy!")
}

// Truthy Values
if(true){
    console.log("Truthy!")
}

if(1){
    console.log("Truthy!")
}

if([]){
    console.log("Truthy!")
}

// Ternary Operators
let result = (1 < 10) ? true : false; //Is 1 lesser than 10? If so, true. If not, false.

// If-Else Equivalent of Ternary Result Variable
// if (1 < 10) {
//     return true;
// } else {
//     return false;
// }
if (5 == 5) {
    let greeting = "Hello!";
    console.log(greeting);
}

console.log("Value returned from the ternary operator is " + result);

// Switch Statements
let day = prompt("What day of the week is it today?").toLowerCase();

switch(day){
    case 'monday':
        console.log("The day today is Monday!");
        break;
    case 'tuesday':
        console.log("The day today is Tuesday!");
        break;
    case 'wednesday':
        console.log("The day today is Wednesday!");
        break;
    case 'thursday':
        console.log("The day today is Thursday!");
        break;
    case 'friday':
        console.log("The day today is Friday!");
        break;
    case 'saturday':
        console.log("The day today is Saturday!");
        break;
    case 'sunday':
        console.log("The day today is Sunday!");
        break;
    default:
        console.log("Please input a valid day smh my head..")
        break;
}

// Try/Catch/Finally Statements
function showIntensityAlert(windSpeed){
    try{
        alertat(determineTyphoonIntensity(windSpeed));
    } catch(error){
        console.log(error.message)
    } finally {
        alert("Intensity updates will show new alert!");
    }
}

showIntensityAlert(56);