db.users.insertOne({
    "firstName": "Jane",
    "lastName": "Doe",
    "age": 21,
    "contact": {
        "phone": "0912345678",
        "email": "janedoe@gmail.com"
    },
    "courses": ["CSS", "Javascript", "Python"],
    "department": "none"
});

// Inserting multiple documents at once
db.users.insertMany([
    {
        "firstName": "John",
        "lastName": "Doe"
    },
    {
        "firstName": "Joseph",
        "lastName": "Doe"
    }
])

// Retrieving all the inserted users
db.users.find();

// Retrieving a specific document from a collection
db.users.find({ "firstName": "John" })

// Updating existing Documents
// For updating a single document.
db.users.updateOne(
    // First part - retrieving a specific user using the ID
    {
        "_id": ObjectId("64c1c0bccb6dd416cb826acf")
    },

    // Second part - uses the $set keyword to set a specific property of that user to a new value.
    {
        $set: {
            "lastName": "Gaza"
        }
    }
)

// For updating multiple documents.
// updateMany() allows for modification of 2 or more documents as compared to updateOne() which can only accomodate of a single document
db.users.updateMany(
    {
        "lastName": "Doe"
    },

    {
        $set: {
            "firstName": "Mary"
        }
    }
)

// Deleting Documents from a Collection
// Deleting multiple documents
db.users.deleteMany({ "lastName": "Doe" });

// Deleting single document
db.users.deleteOne( {"_id": ObjectId("64c1c0bccb6dd416cb826acf")} )