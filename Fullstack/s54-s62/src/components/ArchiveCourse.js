import { Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCourse({course_id, course_name, fetchCourses, isActive}) {
    // Mini Activity (40 Mins)
    // 1. Create a fetch request inside the 'archiveCourse' function that will activate the existing course based on its ID.
    // 2. Create a fetch request inside the 'activateCourse' function that will activate a course that is currently archived.
    // 3. Trigger the 'archiveCourse' function when the Archive button is clicked, same with the Activate button, which should trigger the 'activateCourse' function
    // 4. Once a course has successfully been archived OR activated, show a sweetalert that will alert that user of the success, and trigger the 'fetchCourses' function to update the current list of courses.

    const archiveCourse = (courseId) => {
        fetch(`${process.env.REACT_APP_API_URL}/api/courses/${courseId}/archive`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                isActive: isActive
            })
        }).then(response => response.json())
        .then(result => {
            if(result) {
                Swal.fire({
                    title: "Course Archived!",
                    text: `Course ${course_name} successfully archived.`,
                    icon: 'success'
                })
                fetchCourses();
            } else {
                Swal.fire({
                    title: "Something went wrong.",
                    text: "Please try again.",
                    icon: 'error'
                })
                fetchCourses();
            }
        })
    }

    const activateCourse = (courseId) => {
        fetch(`${process.env.REACT_APP_API_URL}/api/courses/${courseId}/activate`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                isActive: isActive
            })
        }).then(response => response.json())
        .then(result => {
            if(result) {
                Swal.fire({
                    title: "Course Activated!",
                    text: `Course ${course_name} successfully activated.`,
                    icon: 'success'
                })
                fetchCourses();
            } else {
                Swal.fire({
                    title: "Something went wrong.",
                    text: "Please try again.",
                    icon: 'error'
                })
                fetchCourses();
            }
        })
    }

    return (
        <>
            {
                (isActive) ?
                    <Button variant = "warning" size = "sm" onClick = {() => archiveCourse(course_id)}>Archive</Button>
                :
                    <Button variant = "success" size = "sm" onClick = {() => activateCourse(course_id)}>Activate</Button>
            }
        </>
    )
    
}