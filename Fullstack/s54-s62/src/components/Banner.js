import {Button, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner(){
    return(
        <Row>
            <Col className="p-5 text-center">
                <h1>Group1 Gaming Bootcamp</h1>
                <p>Games for everyone!</p>
                <Button variant="primary" as = {Link} to = '/courses'>Enroll Now!</Button>
            </Col>
        </Row>
    )
}