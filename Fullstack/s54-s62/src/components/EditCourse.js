import { Button, Modal, Form } from 'react-bootstrap';
import { useState } from 'react';
import Swal from 'sweetalert2';

export default function EditCourse({course_id, course_name, fetchCourses}) {
    // This state contains the ID of the course that is selected
    const [courseId, setCourseId] = useState('');

    // Form States
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');

    // Model Activation
    const [showEditModal, setShowEditModal] = useState(false);


    const openEditModal = (courseId) => {
        fetch(`${process.env.REACT_APP_API_URL}/api/courses/${courseId}`)
        .then(response => response.json())
        .then(result => {
            //Pre-populate the form input fields with data from the API
            setCourseId(result._id)
            setName(result.name)
            setDescription(result.description)
            setPrice(result.price)
        })

        setShowEditModal(true);
    }

    const closeEditModal = () => {
        setShowEditModal(false)
        setName('')
        setDescription('')
        setPrice('')
    }

    const editCourse = (event, courseId) => {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/api/courses/${courseId}`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(response => response.json())
        .then(result => {
            if(result) {
                Swal.fire({
                    title: "Course Updated!",
                    text: `Course ${course_name} successfully updated.`,
                    icon: 'success'
                })
                // Triggers the fetching of all courses after updaating a course. The 'fetchCourses' function comes from the props that were passed from the
                fetchCourses();
                closeEditModal();
            } else {
                Swal.fire({
                    title: "Something went wrong.",
                    text: "Please try again.",
                    icon: 'error'
                })
                fetchCourses();
                closeEditModal();
            }
        })
        
    }

    return(
        <>
            <Button variant = "primary" size = "sm" onClick = {() => openEditModal(course_id)}>Edit</Button>

            <Modal show = {showEditModal} onHide = {closeEditModal}>
                <Form onSubmit = {event => editCourse(event, courseId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Course</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>

                        <Form.Group controlId="courseName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control 
                            type="text"
                            value = {name} 
                            onChange = {event => setName(event.target.value)}
                            required/>
                        </Form.Group>

                        <Form.Group controlId="courseDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control 
                            type="text" 
                            value = {description} 
                            onChange = {event => setDescription(event.target.value)}
                            required/>
                        </Form.Group>

                        <Form.Group controlId="coursePrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control 
                            type="number" 
                            value = {price} 
                            onChange = {event => setPrice(event.target.value)}
                            required/>
                        </Form.Group>

                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant='secondary' onClick = {closeEditModal}>Close</Button>
                        <Button variant='success' type='submit'>Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}