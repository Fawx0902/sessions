import {Row, Col} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import {useContext, useState, useEffect} from 'react';

export default function Profile(){
    const {user} = useContext(UserContext)

    const [details, setDetails] = useState({})

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: user.id
            })
        })
        .then(response => response.json())
        .then(result => {
            if (typeof result._id !== 'undefined'){
                setDetails(result)
            }
        })
    })
    return(
        (user.id === null) ?
            <Navigate to = '/courses'/>
        :
        <Row className = "my-3">
            <Col xs={12} className = "text-center my-2">
                <h1>My Profile</h1>
            </Col>
            <Col className = "p-5">
                <h2>{`${details.firstName} ${details.lastName}`}</h2>
                <h4>Contact List:</h4>
                <ul>
                    <li>Mail: {`${details.email}`}</li>
                    <li>Mobile Number: {`${details.mobileNo}`}</li>
                </ul>
            </Col>
        </Row>
    )
}