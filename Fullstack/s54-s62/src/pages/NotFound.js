export default function NotFound(){
    return(
        <>
            <div class = "errormsg my-5">
            <h1>Not Found</h1>
            <p class = "my-3">The page you visited could not be found..</p>
            </div>
        </>
    )
}