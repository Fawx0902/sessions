import React from 'react';

// Initializing the context
const UserContext = React.createContext();

// Exporting the provider from the context
export const UserProvider = UserContext.Provider

// Exporting the entire context
export default UserContext;